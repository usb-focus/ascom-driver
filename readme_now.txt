
Thank you for downloading this test app.


This exe file is dedicated to USB_Focus product.

Use it with ASCOM platform to test moves IN and OUT short and long moves.

If a motor is powered and connected to USB_Focus controller, make sure it is not moving your telescope focuser.

This app uses the command line :

1. open cmd.exe
2. at prompt several options are available :

You can access severall tests mode using command line parameters like : testapp.exe -log -inout"
	/?      : this help message
	-log    : all events will be logged into a file in c:\\USB_Focus\\"
	-all    : you can go through all tests, OUT-IN / RANDOM / LONG MOVES (1210 moves) ~~ it can take a while to finish
	-inout  : only test the OUT / IN (200x moves) between positions 0 and 1000 (make sure the motor can move into this range)
	-random : only test random moves (1000x moves) between positions 0 and 1000 (make sure the motor can move into this range)
	-long   : only test long moves (10x moves) between positions 0 and 10000 (make sure the motor can move into this range)



If any error, please report to support@usb-foc.us


Best regards,
Vincent