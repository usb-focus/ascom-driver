Thank you for downloading Ascom UFO v2.64 driver.
It complies with 32 and 64bits systems.

If you use one single UFO controller, please install USB_Focus Ascom driver.

If you use two UFO controllers at the same time, please install in addition to previous one, the USB_FocusBis Ascom driver.


New feature :
- added a pause after move feature to enable motor vibrations to reduce

------------------------

Merci d'avoir t�l�charg� le pilote Ascom USB_Focus version 2.63
Il est compatible 32 bits et 64 bits.

Si vous utilisez un seul controlleur, merci d'installer le pilote USB_Focus.

Si vous utilisez deux controlleurs en m�me temps, installez en plus du pr�c�dent pilote, le pilote Ascom USB_FocusBis.

Nouvelle fonction :
- ajout d'un temps de pause apr�s chaque d�placement, afin de diminuer les effets de vibration



Happy focus !

Vincent

www.usb-foc.us