﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace ASCOM.USB_FocusBis
{
    [ComVisible(false)]					// Form not registered for COM!
    public partial class SetupDialogForm : Form
    {
        public SetupDialogForm()
        {
            InitializeComponent();

            comboBoxComPort.Items.Clear();
            using(ASCOM.Utilities.Serial    serial = new Utilities.Serial())
            {
                foreach(var item in serial.AvailableCOMPorts)
                {
                    comboBoxComPort.Items.Add(item);
                }

                // auto select default comm port if available
                try
                {
                    this.comboBoxComPort.SelectedItem = Properties.Settings.Default.CommPort;
                }
                catch{}

                try
                {
                    this.checkBoxBacklash.Checked = Properties.Settings.Default.isBacklashEnabled;
                }
                catch { }

                try
                {
                    this.maskedTextBoxBacklash.Text = "" + Properties.Settings.Default.backlashValue;
                }
                catch {  }

                try
                {
                    this.comboBoxBacklashDir.SelectedItem = Properties.Settings.Default.backlashDir;
                }
                catch { }

                try
                {
                    this.textBoxStepSize.Text = "" + Properties.Settings.Default.stepSize;
                }
                catch { }

                try
                {
                    this.maskedTextBoxMaxIncrement.Text = "" + Properties.Settings.Default.MaxIncrement;
                }
                catch { }

                try
                {
                    this.maskedTextBoxMaxPosition.Text = "" + Properties.Settings.Default.MaxStep;
                }
                catch { }

                try
                {
                    this.checkBoxLog.Checked = Properties.Settings.Default.isLogEnabled;
                }
                catch { }

            }
        }

        // SetupDialog UI form OK button
        private void cmdOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.CommPort = (string)this.comboBoxComPort.SelectedItem;
            Properties.Settings.Default.isBacklashEnabled = this.checkBoxBacklash.Checked;
            Properties.Settings.Default.backlashValue = Int32.Parse(this.maskedTextBoxBacklash.Text);
            Properties.Settings.Default.backlashDir = (String)this.comboBoxBacklashDir.SelectedItem;
            Properties.Settings.Default.isLogEnabled = this.checkBoxLog.Checked;

            // Save stepsize in BDD
            //Properties.Settings.Default.stepSize = Double.Parse(this.textBoxStepSize.Text, CultureInfo.InvariantCulture.NumberFormat);

            string decimalPoint = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
            if (!this.textBoxStepSize.Text.Contains(decimalPoint))
            {
                this.textBoxStepSize.Text = this.textBoxStepSize.Text.Replace(".", decimalPoint);
                this.textBoxStepSize.Text = this.textBoxStepSize.Text.Replace(",", decimalPoint);
            }

            Properties.Settings.Default.stepSize = double.Parse(this.textBoxStepSize.Text);


            // Save maxIncrement in BDD
            int maxIncrement = Int32.Parse(this.maskedTextBoxMaxIncrement.Text);
            if (maxIncrement > 0 && maxIncrement <= 65535)
                Properties.Settings.Default.MaxIncrement = maxIncrement;
            else
                Properties.Settings.Default.MaxIncrement = 50000;

            // Save MaxStep in BDD
            int maxStep = Int32.Parse(this.maskedTextBoxMaxPosition.Text);
            if (maxStep > 0 && maxStep <= 65535)
                Properties.Settings.Default.MaxStep = maxStep;
            else
                Properties.Settings.Default.MaxStep = 50000;

            Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BrowseToAscom(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://ascom-standards.org/");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
        }

    }
}