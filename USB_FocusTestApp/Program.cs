﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB_FocusTestApp
{
    class Program
    {

        public static ASCOM.DriverAccess.Focuser focuser;
        public static string id;

        static void Main(string[] args)
        {
            id = ASCOM.DriverAccess.Focuser.Choose("");

    #region I N I T I L I Z A T I O N
            /// P R O P E R T I E S
            focuser = new ASCOM.DriverAccess.Focuser(id);
            Console.WriteLine("name " + focuser.Name);
            Console.WriteLine("description " + focuser.Description);
            Console.WriteLine("DriverInfo " + focuser.DriverInfo);
            Console.WriteLine("driverVersion " + focuser.DriverVersion);
            Console.WriteLine("Absolute Focuser " + focuser.Absolute);
            Console.WriteLine("StepSize " + focuser.StepSize);
            Console.WriteLine("TempComp available " + focuser.TempCompAvailable);
            Console.WriteLine("TempComp status " + focuser.TempComp);
            Console.WriteLine("Connected " + focuser.Connected);


            /// O P E N  C O N N E C T I O N
            Console.WriteLine("");
            focuser.Connected = true;
            Console.WriteLine("Connected " + focuser.Connected);


            /// R E T R I E V E    M A X   P A R A M E T E R S

            Console.WriteLine("");
            Console.WriteLine("MaxIncrement = " + focuser.MaxIncrement);
            Console.WriteLine("MaxStep = " + focuser.MaxStep);
            Console.WriteLine("");
            Console.WriteLine("Position = " + focuser.Position);
            Console.WriteLine("Temperature = " + focuser.Temperature); 
            #endregion

    #region T E M P E R A T U R E

            /**       
            focuser.TempComp = true;

            Console.WriteLine("Move to 2500 with Temcomp, should throw : ASCOM::InvalidOperationException");
            try
            {
                focuser.Move(2500);
            }
            catch
            {
                Console.WriteLine("Move while TempComp active Impossible !");
            }
            Console.WriteLine("Position =" + focuser.Position);
            focuser.TempComp = false;
            Console.WriteLine("TempComp turned :" + focuser.TempComp);
            */
            
            #endregion

    #region P O S I T I O N  /  M O V E S
    /*
            Console.WriteLine("");
            Console.WriteLine("Position :" + focuser.Position);
            Console.WriteLine("IsMoving :" + focuser.IsMoving);
            Console.Write("");
            Console.WriteLine("Move to position 1000");
            Console.Write(">");
            focuser.Move(1000);
            while(focuser.IsMoving)
            {
                Console.Write(".");
                //Console.WriteLine(".  New position :" + focuser.Position);
                System.Threading.Thread.Sleep(500);
            }
            Console.Write(">");
            Console.WriteLine("");
            Console.WriteLine("Reached position :" + focuser.Position);
            Console.WriteLine("");
            Console.WriteLine("Move to position 2000");
            Console.Write(">");
            focuser.Move(2000);
            while (focuser.IsMoving)
            {
                Console.Write(".");
                System.Threading.Thread.Sleep(500);
            }
            Console.Write(">");
            Console.WriteLine("");
            Console.WriteLine("Reached position :" + focuser.Position);
            Console.WriteLine("");

            // OUT LIMITS TESTS
            Console.WriteLine("Move to position -100");
            try
            {
                focuser.Move(-100);
            }
            catch
            {
                Console.WriteLine("Move to position -100 Impossible !");
            }
            Console.WriteLine("Move to position 99999");
            try
            {
                focuser.Move(99999);
            }
            catch
            {
                Console.WriteLine("Move to position 99999 Impossible !");
            }
      */ 
	#endregion      

    #region T E S T S   B A C K L A S H   I N T E R F A C E
            //Console.WriteLine("Backlash activé :" + focuser.BacklashState);
            //Console.WriteLine("Backlash activé :" + focuser.BacklashCount); 
	#endregion

    #region B R U T   F O R C E   T E S T S  1
    /*
    Console.WriteLine("");
    Console.WriteLine("Position initiale :" + focuser.Position);
            
    int i = 10;

    while (i >= 1)
    {
        --i;

        focuser.Move(2000+i*100);
        while (focuser.IsMoving)
        {
            Console.Write(".");
            System.Threading.Thread.Sleep(400);
        }
        Console.Write(">");
        Console.WriteLine("");
        Console.WriteLine("Reached position :" + focuser.Position);
        Console.WriteLine("");


        focuser.Move(2000-i*100);
        while (focuser.IsMoving)
        {
            Console.Write(".");
            System.Threading.Thread.Sleep(400);
        }
        Console.Write(">");
        Console.WriteLine("");
        Console.WriteLine("Reached position :" + focuser.Position);
        Console.WriteLine("");

    }

    Console.WriteLine("");
    Console.WriteLine("Position finale :" + focuser.Position);
            */ 
	#endregion

    #region B R U T   F O R C E   T E S T S  2

            writelines(0);

            focuser.Move(0);
            while (focuser.IsMoving)
            {
                Console.WriteLine("[target: 0] Reached position :" + focuser.Position+ " / last known T°:" + focuser.Temperature);
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("[target: 0] Reached position :" + focuser.Position + " / current T°:" + focuser.Temperature);


            writelines(10000);

            focuser.Move(10000);
            while (focuser.IsMoving)
            {
                Console.WriteLine("[OUT target: 10000] Reached position :" + focuser.Position + " / last known T°:" + focuser.Temperature);
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("[OUT target: 10000] Reached position :" + focuser.Position + " / current T°:" + focuser.Temperature);



            writelines(0);

            int j=0;

            focuser.Move(focuser.Position - 100);
            Console.WriteLine("[move IN #" + j + "] Reached position :" + focuser.Position + " / T°:" + focuser.Temperature);

            /*while (focuser.Position > 100)
            {
                ++j;

                focuser.Move(focuser.Position - 100);
                while (focuser.IsMoving)
                {
                    Console.Write(".");
                    System.Threading.Thread.Sleep(100);
                }
                Console.WriteLine("[move IN #" + j + "] Reached position :" + focuser.Position + " / T°:" + focuser.Temperature);

                System.Threading.Thread.Sleep(1000);
            }*/ 
	#endregion      

    #region B R U T   F O R C E  T E S T S  3 - Max moves
            /**
            Console.WriteLine("");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("USB_Focus va se déplacer jusque position 0, Press Enter...");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("");
            Console.ReadLine();

            focuser.Move(0);
            while (focuser.IsMoving)
            {
                Console.WriteLine("[IN target: 0] Reached position : is moving / last known T°:" + focuser.Temperature);
                System.Threading.Thread.Sleep(2000);
            }
            Console.Write(">");
            Console.WriteLine("[OUT target: 0] Reached position :" + focuser.Position + " / current T°:" + focuser.Temperature);

            Console.WriteLine("");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("USB_Focus va se déplacer jusque sa position max (MaxStep), Press Enter...");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("");
            Console.ReadLine();

            focuser.Move(focuser.MaxStep);
            while (focuser.IsMoving)
            {
                Console.WriteLine("[OUT target:" + focuser.MaxStep + "] Reached position : is moving / last known T°:" + focuser.Temperature);
                System.Threading.Thread.Sleep(2000);
            }
            Console.Write(">");
            Console.WriteLine("[OUT target:" + focuser.MaxStep + "] Reached position :" + focuser.Position + " / current T°:" + focuser.Temperature);

            //----

            Console.WriteLine("");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("USB_Focus va se déplacer jusque position 0, Press Enter...");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("");
            Console.ReadLine();

            focuser.Move(0);
            while (focuser.IsMoving)
            {
                Console.WriteLine("[IN target: 0] Reached position : is moving / last known T°:" + focuser.Temperature);
                System.Threading.Thread.Sleep(2000);
            }
            Console.Write(">");
            Console.WriteLine("[IN target: 0] Reached position :" + focuser.Position + " / current T°:" + focuser.Temperature);

            // B R U T   F O R C E  T E S T S  2 

            // Single test get position
            //Console.WriteLine("Position :" + focuser.Position);
            
             * **/ 
	#endregion

    #region C L O S E  C O N N E C T I O N
            Console.WriteLine("");
            Console.WriteLine("Close connection.");
            focuser.Connected = false;

            Console.WriteLine("");
            Console.WriteLine("Press Enter to finish");
            Console.ReadLine(); 
	#endregion
        
        }

        static public void writelines(int newpos)
        {
            Console.WriteLine("");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("USB_Focus will move to position %i, Press Enter...", newpos);
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("");
            Console.ReadLine();
        }


    }
}
