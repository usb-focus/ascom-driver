﻿//tabs=4
// --------------------------------------------------------------------------------
//
// ASCOM Focuser driver for USB_Focus
//
// Description:	USB_Focus focuser DotNet driver
//
// Implements:	ASCOM Focuser interface version: 2.0
// Author:		Vincent Auvray  <vincent@usb-foc.us>
//
// Edit Log:
//
// Date			Who	Vers	Description
// -----------	---	-----	-------------------------------------------------------
// 08-04-2013	Vincent Auvray	6.0.0	Initial edit, created from ASCOM driver template
// --------------------------------------------------------------------------------
//

/*
 * FAMODE : A + P=wxyz LFCR + 10ms + T=+/-xy.z LFCR   /// MSG_FPOSRO + MSG_FTMPRO   => FAUTOM
 * FBMODE : B + P=wxyz LFCR + 10ms + T=+/-xy.z LFCR   /// MSG_FPOSRO + MSG_FTMPRO
 * FFMODE : END LFCR       /// MSG_FFMODE      _
 * FLAxyz : DONE LFCR      /// MSG_DONE                                 => FLXnnn
 * FMMODE : ! LFCR         /// MSG_FMMODE=> FMANUA
 * FPOSRO : P=vwxyz LFCR   /// MSG_FPOSRO      _
 * FQUITx : DONE LFCR      /// MSG_DONE        _
 * FREADA : A=0xyz LFCR    /// MSG_FREADA      _
 * FTMPRO : T=+/-xy.z LFCR /// MSG_FTMPRO      _
 * FTxxxA : A=x LFCR       /// MSG_FTA         _ // un seul coef géré
 * FZAxxn : DONE LFCR      /// MSG_DONE        _ // un seul coef géré         => FZSIGn
 *
 * SSBOOT : Reboot le module
 * SBOOTL : reboot in bootloader mode (0xFFFF EEPROM address 0xFE and 0xFF), once flashed, software RESET button sets 0x0
 * SEERAZ : Efface l'EEPROM
 * SEEGET : retourne les valeurs en EEPROM
 * SMAnnn : Modifie le nombre de pas à partir duquel en prend en compte le déplacement moteur (mode auto)
 * SMO00n : Set la vitesse entre pas moteur (multiples de 1ms) entre 0 et 9 (4 par default)
 * SMROTH : Modifie le sens de rotation du moteur : Horaire
 * SMROTT : Modifie le sens de rotation du moteur : Trigo
 * SMSTPF : Modifie le déplacement pas entier
 * SMSTPD : Modifie le déplacement demi/pas (L298/L293D)
 * SPADOK : Indique si le PAD est connecté
 * SPADKO : Indique si le PAD est deconnecté
 * SGETAL : renvoie les valeurs de config en EEPROM : C=[rotation]-[stepmode]-[motor speed]-[Temp coeff]-[Temp comp mini]-[fw version]-[maxpos]
 * Mnnnnn : Modifie le nombre de pas maxi calibré par l'utilisateur. Entre 00000 et 65535
 * Innnnn : déplace en intra de 65535 pas maxi
 * Onnnnn : déplace en extra de 65535 pas maxi
 * 
 * SWHOIS : renvoie UDP ou UFO
 * 
 * DEPRECATED
 * FDAxyz : DONE LFCR      /// MSG_DONE
 * FDBxyz : DONE LFCR      /// MSG_DONE
 * FHOME  : DONE LFCR      /// MSG_DONE        _
 * FWAKUP : WAKE LFCR      /// MSG_FWAKUP      _
 * FCENTR : CENTER LFCR    /// MSG_FCENTR      _
 * FSLEEP : ZZZ LFCR       /// MSG_FSLEEP      _
 * SVERxx : deprecated - voir SGETAL
 * SSHELP : Menu d'aide
 * SDBGON : enable traces
 * SDBGOF : disable traces
 * TTEMPE : tests temperature
 * TMOTOR : tests moteur
 * TTLEDS : tests des LEDs
 */

#define Focuser

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

using ASCOM;
using ASCOM.Utilities;
using ASCOM.DeviceInterface;
using System.Globalization;
using System.Collections;
using System.IO;

namespace ASCOM.USB_Focus
{
    //
    // Your driver's DeviceID is ASCOM.USB_Focus.Focuser
    //

    /// <summary>
    /// ASCOM Focuser Driver for USB_Focus.
    /// </summary>
    [Guid("a4b0895b-62a3-43ce-a1de-155ac257802a")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Focuser : IFocuserV2
    {
        /// <summary>
        /// ASCOM DeviceID (COM ProgID) for this driver.
        /// The DeviceID is used by ASCOM applications to load the driver at runtime.
        /// </summary>
        public static string driverID = "ASCOM.USB_Focus.Focuser";
        /// <summary>
        /// Driver description that displays in the ASCOM Chooser.
        /// </summary>

        private static string driverVersion = "v2.6 test";
        public static string driverDescription = "Ascom USB_Focus DotNet "+driverVersion;

        private Serial  serialPort;

        private Util HC = new Util();

        private string comPort;

        private bool bMoving = false;       // Si true déplacement en cours

        private bool bTempComp = false;     // Si true Temperature compensation actif

        private int iMaxStep;               // Valeur de position max, en nbr de pas ou demis-pas

        private bool isBacklashEnabled = false; // Si true prise en compte des valeurs de backlash

        private int backlashValue;              // Valeur du backlash en pas ou demis-pas

        private bool backlashDir;               // Direction vers dans laquelle on prendra en compte le backlash

        private bool isLogEnabled = false;      // Si true prise en compte des valeurs de backlash

        private int iPosition = 0;              // Store last known position (used to feed client app with last position when waiting thread to release comm port)
        private double dTemp = 0;               // Store last known temp (used to feed client app with last temp when waiting thread to release comm port)

        Thread move_thread;                     // Thread to manage MOVE command (ACK) to get non-blocking operations

        private int PID = 0;                    //identifiant de log Position
        private int TID = 0;                    //identifiant de log temperature

        private string specificFolder = "";     // User application data folder
        private string logFilePath = "";        // Log file path

#if Telescope
        //
        // Driver private data (rate collections) for the telescope driver only.
        // This can be removed for other driver types
        //
        private readonly AxisRates[] _axisRates;
#endif
        /// <summary>
        /// Initializes a new instance of the <see cref="USB_Focus"/> class.
        /// Must be public for COM registration.
        /// </summary>
        public Focuser()
        {
            //TODO: Implement your additional construction here
            
            /////////// LOG FILE
            // The folder for the roaming current user 
            //string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData); // chemin standard
            string folder = "c:\\"; // chemin pour Mr ..

            // Combine the base folder with your specific folder....
            specificFolder = Path.Combine(folder, "USB_Focus");

            // Check if folder exists and if not, create it
            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            //Find unix timestamp (seconds since 01/01/1970)
            long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks;
            ticks /= 1000; //Convert windows ticks to seconds

            // Create final log file path
            logFilePath = Path.Combine(specificFolder, "USB_Focus_log_"+ticks.ToString()+".txt");

            /////////// LOG FILE


            //start thread to get ACK of move ended from focuser without blocking the driver
            move_thread = new Thread(new ThreadStart(moveAck));
            move_thread.Start();
        }

        #region ASCOM Registration
        //
        // Register or unregister driver for ASCOM. This is harmless if already
        // registered or unregistered. 
        //
        /// <summary>
        /// Register or unregister the driver with the ASCOM Platform.
        /// This is harmless if the driver is already registered/unregistered.
        /// </summary>
        /// <param name="bRegister">If <c>true</c>, registers the driver, otherwise unregisters it.</param>
        private static void RegUnregASCOM(bool bRegister)
        {
            using (var P = new ASCOM.Utilities.Profile())
            {
                P.DeviceType = "Focuser";
                if (bRegister)
                {
                    P.Register(driverID, driverDescription);
                }
                else
                {
                    P.Unregister(driverID);
                }
            }
        }

        /// <summary>
        /// This function registers the driver with the ASCOM Chooser and
        /// is called automatically whenever this class is registered for COM Interop.
        /// </summary>
        /// <param name="t">Type of the class being registered, not used.</param>
        /// <remarks>
        /// This method typically runs in two distinct situations:
        /// <list type="numbered">
        /// <item>
        /// In Visual Studio, when the project is successfully built.
        /// For this to work correctly, the option <c>Register for COM Interop</c>
        /// must be enabled in the project settings.
        /// </item>
        /// <item>During setup, when the installer registers the assembly for COM Interop.</item>
        /// </list>
        /// This technique should mean that it is never necessary to manually register a driver with ASCOM.
        /// </remarks>
        [ComRegisterFunction]
        public static void RegisterASCOM(Type t)
        {
            RegUnregASCOM(true);
        }

        /// <summary>
        /// This function unregisters the driver from the ASCOM Chooser and
        /// is called automatically whenever this class is unregistered from COM Interop.
        /// </summary>
        /// <param name="t">Type of the class being registered, not used.</param>
        /// <remarks>
        /// This method typically runs in two distinct situations:
        /// <list type="numbered">
        /// <item>
        /// In Visual Studio, when the project is cleaned or prior to rebuilding.
        /// For this to work correctly, the option <c>Register for COM Interop</c>
        /// must be enabled in the project settings.
        /// </item>
        /// <item>During uninstall, when the installer unregisters the assembly from COM Interop.</item>
        /// </list>
        /// This technique should mean that it is never necessary to manually unregister a driver from ASCOM.
        /// </remarks>
        [ComUnregisterFunction]
        public static void UnregisterASCOM(Type t)
        {
            RegUnregASCOM(false);
        }
        #endregion

        //
        // PUBLIC COM INTERFACE IFocuserV2 IMPLEMENTATION
        //

        /// <summary>
        /// Displays the Setup Dialog form.
        /// If the user clicks the OK button to dismiss the form, then
        /// the new settings are saved, otherwise the old values are reloaded.
        /// THIS IS THE ONLY PLACE WHERE SHOWING USER INTERFACE IS ALLOWED!
        /// </summary>
        public void SetupDialog()
        {
            if (IsConnected)
                System.Windows.Forms.MessageBox.Show("USB_Focus already connected, please disconnect before modifying parameters, please press OK");

            using (SetupDialogForm F = new SetupDialogForm())
            {
                var result = F.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Properties.Settings.Default.Save();

                    return;
                }
                Properties.Settings.Default.Reload();
            }
        }

        #region common properties and methods. All set to no action

        public System.Collections.ArrayList SupportedActions
        {
            get { return new ArrayList(); }
        }

        public string Action(string actionName, string actionParameters)
        {
            throw new ASCOM.MethodNotImplementedException("Action");
        }

        public void CommandBlind(string command, bool raw)
        {
            CheckConnected("CommandBlind");
            // Call CommandString and return as soon as it finishes
            this.CommandString(command, raw);
            // or
            throw new ASCOM.MethodNotImplementedException("CommandBlind");
        }

        public bool CommandBool(string command, bool raw)
        {
            CheckConnected("CommandBool");
            string ret = CommandString(command, raw);
            // TODO decode the return string and return true or false
            // or
            throw new ASCOM.MethodNotImplementedException("CommandBool");
        }

        public string CommandString(string command, bool raw)
        {
            CheckConnected("CommandString");
            
            if(!this.Connected)
                throw new ASCOM.NotConnectedException();

            serialPort.ClearBuffers();
            serialPort.Transmit(command);

            serialPort.ReceiveTimeout = 5;  // Timeout 3 sec
                                            // move_thread thread handles longer Move commands
            String st = "";
            
            if (!(command.StartsWith("I") || command.StartsWith("O")))//Case not a Move command
            {
                st = serialPort.ReceiveTerminated("\n\r");   // Wait for terminated character
            }

            return st;
        }

        // Threaded to handle blocking time to move focuser
        public void moveAck()
        {
            try{
                while(true)
                {
                    Thread.Sleep(100);  //polling serial port each xxxms

                    if (bMoving)    // if focuser moves
                    {
                        String st="";

                        try
                        {
                            st = serialPort.Receive();   // blocking read function up to timeout
                                }
                        catch { }

                        if (st.Contains("*"))   // if focuser sends ACK command
                            bMoving = false;    // Move ended notified to client app
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                System.Threading.Thread.ResetAbort();
            }
        }

        #endregion

        #region public properties and methods
        public void Dispose(){       }

        public bool Connected
        {
            get { return IsConnected; }
            set
            {
                this.isLogEnabled = Properties.Settings.Default.isLogEnabled;   // To Log or not to Log
                if (isLogEnabled)
                    System.Windows.Forms.MessageBox.Show("USB_Focus ready to log activities into :" + specificFolder);

                if (value == IsConnected)
                    return;

                if (value)
                {
                    comPort = Properties.Settings.Default.CommPort;

                    try{
                        serialPort =new Serial();
                        serialPort.PortName = comPort;
                        serialPort.Speed =SerialSpeed.ps9600;
                        serialPort.Connected =true;
                    } catch (Exception ex)
                    {
                        throw new ASCOM.NotConnectedException("Serial port connection error:", ex);
                    }

                    
                    try
                    {
                        if (isLogEnabled)
                            File.AppendAllText(logFilePath, "\r\n---------------------------------------------------------------------------------------INIT\r\n");
                        
                        //Get firmware and Ascom driver version
                        File.AppendAllText(logFilePath, "\r\n Local settings : "+ driverDescription+ ", USB_Focus software details :");
                        
                        getEEPROMvalues();                                  // retrieve all USB_Focus internal parameters

                        iPosition = this.Position;                          // Get current position and temp from Focuser
                        dTemp = this.Temperature;                           // Get current temp from Focuser
                        MaxStep = Properties.Settings.Default.MaxStep;      // Save MaxPosition in Focuser at connection startup
                        if (isLogEnabled)
                            File.AppendAllText(logFilePath, "\r\n---------------------------------------------------------------------------------------INIT\r\n");

                    } catch (Exception ex)
                    {
                        throw new ASCOM.NotConnectedException("Serial port connection error:", ex);
                    }

                }
                else
                {
                    if(serialPort !=null && serialPort.Connected)
                        serialPort.Connected = false;
                }
            }
        }

        public string Description
        {
            get { return driverDescription; }
        }

        public string DriverInfo
        {
            get { return driverDescription; }
        }

        public string DriverVersion
        {
            get
            {
                Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                return String.Format(CultureInfo.InvariantCulture, "{0}.{1}", version.Major, version.Minor);
            }
        }

        public short InterfaceVersion
        {
            // set by the driver wizard
            get { return 2; }
        }

        #endregion

        #region private properties and methods

        /// <summary>
        /// Returns true if there is a valid connection to the driver hardware
        /// </summary>
        private bool IsConnected
        {
            get
            {
                // TODO check that the driver hardware connection exists and is connected to the hardware
                if(serialPort ==null)
                    return false;
                return serialPort.Connected;
            }
        }

        /// <summary>
        /// Use this function to throw an exception if we aren't connected to the hardware
        /// </summary>
        /// <param name="message"></param>
        private void CheckConnected(string message)
        {
            if (!IsConnected)
            {
                throw new ASCOM.NotConnectedException(message);
            }
        }

        /// <summary>
        /// this function returns the EEPROM parameters from connected USB_Focus
        /// </summary>
        private void getEEPROMvalues()
        {
            string ret="";

            if(this.Connected)
            {
                ret += CommandString("SGETAL", true);
                if (isLogEnabled)
                    File.AppendAllText(logFilePath, ret);
            } 
            else
            {
                throw new ASCOM.NotConnectedException("getEEPROMvalues");
            }
            
        }

        
        #endregion

        #region IFocuserV2 Members

        public bool Absolute
        {
            get { return true; }
        }

        public void Halt()
        {
            //FQUITx
            if(this.Connected)
                if (bMoving)
                {
                    bMoving = false;                                    // stop move thread
                    CommandString(string.Format("FQUITx"), false);      // send FQUITx cmd to focuser
                }
                else
                {
                    return;
                }
            else
            {
                throw new ASCOM.NotConnectedException("Halt");
            }
        }

        public bool IsMoving
        {
            get { return this.bMoving; }
        }

        public bool Link
        {
            get{    return  this.Connected; }
            set{    this.Connected =value;  }
        }

        public double StepSize
        {
            get { 
                
                return Properties.Settings.Default.stepSize;
            
            }
        }

        public int MaxIncrement
        {
            get { return Properties.Settings.Default.MaxIncrement; }
            set {

                if(value>1 || value<=this.MaxStep)
                    Properties.Settings.Default.MaxIncrement = value;
                else
                        throw new ASCOM.InvalidValueException("MaxIncrement value out of range [min = 1, max = current MaxStep].");
            }
        }

        public int MaxStep
        {
            get { return Properties.Settings.Default.MaxStep; }
            set {
                Properties.Settings.Default.MaxStep = value;

                if (this.Connected)
                {
                    if(value>this.iPosition && value<=65535) // MaxStep compris entre position actuelle et 65535
                        CommandString(string.Format("M{0:00000}", value), false);
                    else
                        throw new ASCOM.InvalidValueException("MaxStep value out of range [min = current position, max = 65535].");
                }
                else
                {
                    throw new ASCOM.NotConnectedException("MaxStep");
                }
            }
        }

        public bool BacklashState
        {
            get { return Properties.Settings.Default.isBacklashEnabled; }
            set { Properties.Settings.Default.isBacklashEnabled = value; }
        }

        public int BacklashCount
        {
            get { return Properties.Settings.Default.backlashValue; }
            set { Properties.Settings.Default.backlashValue = value; }
        }

        public bool BacklashDirection
        {
            get {
                if (Properties.Settings.Default.backlashDir.CompareTo("In") > 0)
                    backlashDir = true;
                else
                    backlashDir = false; 
                
                return this.backlashDir; 
            }
            set {
                if(value.Equals(false)){
                    Properties.Settings.Default.backlashDir = "Out";
                }
                else if(value.Equals(true)){
                    Properties.Settings.Default.backlashDir = "In";
                }
            }
        }

        public void Move(int targetPosition)
        {
            // Get parameters values from BDD
            isBacklashEnabled = Properties.Settings.Default.isBacklashEnabled;
            backlashValue = Properties.Settings.Default.backlashValue;
            iMaxStep = Properties.Settings.Default.MaxStep;

            
            // No direct MOVE while Tempcomp
            if (this.bTempComp)
                throw new ASCOM.InvalidOperationException();  //TempComp mode running, disable it first

            // No MOVE while moving
            if (bMoving)
                throw new ASCOM.InvalidOperationException("Focuser is moving, try again");

            // Checks out of range target position
            if (targetPosition < 0 || targetPosition > this.iMaxStep)
                throw new ASCOM.InvalidOperationException("Move to position out of range (0 to MaxStep)");

            // Checks if target position != actual position
            if (targetPosition == this.iPosition)
                return;

            
            // Backlash direction text menus to Bool
            if (Properties.Settings.Default.backlashDir.CompareTo("In") > 0)
                backlashDir = true;    // IN
            else
                backlashDir = false;     // OUT
            
                
                // Simple try to manage backlash in one direction or the other
            // Focusmax :
            // Backlash IN :
            //      when moves IN, no BL compensation
            //      when moves OUT, BL comensation : MOVE OUT to new position + backlash, then MOVE IN to new position - backlash
            // Exemple
            //      position actuelle 1000 et backlashValue 40 //-> Move to 1200
            //      TrueMove (1000+200+40) soit [Move Out 240] // nouvelle position 1240
            //      Truemove 1240-40 [Move In 40] // nouvelle position 1200

            // Backlash OUT :
            //      when moves OUT, no BL compensation
            //      when moves IN, BL compensation : MOVE IN to new position - backlash, then MOVE OUT to new position + backlash
            // Exemple
            //      position actuelle 1000 et backlashValue 40 //-> Move to 800
            //      TrueMove (1000-200-40) soit [Move In 240] // nouvelle position 760
            //      Truemove 760+40 [Move Out 40] // nouvelle position 800

            if (isBacklashEnabled
                && !backlashDir
                && targetPosition > this.iPosition
                && targetPosition + backlashValue <= this.iMaxStep)
                //Backlash enabled and Backlash In and Move Out and position cible + backlash <= position max
            {
                // Debug
                //System.Windows.Forms.MessageBox.Show("Backlash enabled and Backlash In and Move Out\r\rUSB_Focus settings : \risBacklashEnabled:" + isBacklashEnabled + "\rbacklashDir:" + Properties.Settings.Default.backlashDir + "\rbacklashValue:" + backlashValue + "\riMaxStep:" + iMaxStep);

                while (bMoving)
                    HC.WaitForMilliseconds(100);

                TrueMove(targetPosition + backlashValue);

                while (bMoving)
                    HC.WaitForMilliseconds(100);

                // update iPosition value
                int i = this.Position;
                
                TrueMove(targetPosition);

            }
            else if (isBacklashEnabled
                     && backlashDir
                     && targetPosition < this.iPosition 
                     && targetPosition - this.backlashValue >= 0)
                //Backlash enabled and Backlash Out and Move In and position cible - backlash >= 0
            {
                // Debug
                //System.Windows.Forms.MessageBox.Show("Backlash enabled and Backlash Out and Move In\r\rUSB_Focus settings : \risBacklashEnabled:" + isBacklashEnabled + "\rbacklashDir:" + Properties.Settings.Default.backlashDir + "\rbacklashValue:" + backlashValue + "\riMaxStep:" + iMaxStep);

                while (bMoving)
                    HC.WaitForMilliseconds(100);

                TrueMove(targetPosition - this.backlashValue);

                while (bMoving)
                    HC.WaitForMilliseconds(100);

                // update iPosition value
                int i = this.Position;

                TrueMove(targetPosition);

                // position actuelle 4570 et backlashValue 40   //-> déplacement Out 4800
                // TrueMove (4800+40) 4840 [Out de 270] // nouvelle position 4840
                // Truemove 4800 [In de 40]
            }
            else//Backlash disabled
            {
                TrueMove(targetPosition);
            }

            //TrueMove(targetPosition);
        }


        // Final hardware move
        private void TrueMove(int targetPosition)
        {
            int delta = targetPosition - this.iPosition;

            bMoving = true;

            if (delta >= 0)
            {
                // Debug
                //System.Windows.Forms.MessageBox.Show("target position :" + targetPosition + ", current position :" + this.iPosition + ", move OUT:" + delta);
                CommandString(string.Format("O{0:00000}", delta), true);
            }
            else if (delta < 0)
            {
                // Debug
                //System.Windows.Forms.MessageBox.Show("target position :" + targetPosition + ", current position :" + this.iPosition + ", move IN:" + -delta);
                CommandString(string.Format("I{0:00000}", -delta), true);
            }
        }

        public string Name
        {
            get{return  driverDescription;}
        }

        // Update 9/7/2014 : Fonction originale, se désynchronise de temps en temps..
        /**
[09/07/2014 08:12:43] [DEBUG] [CP Update Thread] ASCOM Focuser: Error in GetCurrentPosition. : Exception has been thrown by the target of an invocation. (System.Runtime.InteropServices.COMException (0x80040401): Position value wrong format. Message received from hardware :=+21.)
   at System.RuntimeType.InvokeDispMethod(String name, BindingFlags invokeAttr, Object target, Object[] args, Boolean[] byrefModifiers, Int32 culture, String[] namedParameters)
   at System.RuntimeType.InvokeMember(String name, BindingFlags bindingFlags, Binder binder, Object target, Object[] providedArgs, ParameterModifier[] modifiers, CultureInfo culture, String[] namedParams)
   at SequenceGenerator.SafeFocuser.get_Position()
   at at.GetCurrentPosition(Boolean force)
          
        public int Position
        {
            get{
                string ret = "";

                try
                {
                    if(bMoving)
                        return iPosition;

                    // position command is FPOSRO, returns P=vwxyzLFCR
                    ret = CommandString("FPOSRO", true);
                    int indexP = ret.IndexOf("P=");
                    ret = ret.Substring(indexP + 2, 5);
                    iPosition = int.Parse(ret);

                    return iPosition;
                }
                catch(Exception ex)
                {
                    throw new ASCOM.InvalidValueException("Position value wrong format. Message received from hardware :" + ret, ex);
                }
            }
        }
        */

        public int Position
        {
            get
            {
                int j = 0;
                string ret = "";
                int indexP = 0;
                int indexEnd = 0;
                int newPos = 0;

                string msgLog = "";

                PID++;

                try
                {
                    //if (bMoving)
                    //    return iPosition;

                    msgLog+="\r\nPID" + PID + "---------------------------------------------------------------------------------------\r\n";

                    while (j<=5)
                    {

                        // position command is FPOSRO, returns P=vwxyzLFCR
                        try
                        {
                            ret = CommandString("FPOSRO", true);
                        }
                        catch
                        {
                            // ERROR 0 : Command with no answer Exception catched
                            msgLog+="\r\nPID" + PID + " ER0, retry\r\n";
                        }
                        
                        msgLog+="PID" + PID + " " + DateTime.Now.ToLocalTime() + " GetPosition #try" + j + " Pos:" + ret + "##, P:" + ret.IndexOf("P=") + "\\r\\n" + ret.IndexOf("\n\r")+ "length:"+ret.Length+" ";

                        if (ret.IndexOf("P=") != -1 && ret.IndexOf("\n\r") != -1 && ret.IndexOf("P=") < ret.IndexOf("\n\r") && !ret.Contains("*"))
                        {
                            indexP = ret.IndexOf("P=");
                            indexEnd = ret.IndexOf("\n\r");
                            ret = ret.Substring(indexP + 2, indexEnd - indexP);
                            //msgLog += "DEBUG Champs position=" + ret +" Longueur="+ ret.Length;
                            if (ret.Length == 7)
                            {
                                newPos = int.Parse(ret);

                                msgLog += "PID" + PID + " good format";

                                if (newPos >= 0 && newPos <= MaxStep)
                                {
                                    iPosition = newPos;
                                    msgLog += "PID" + PID + ", good value :" + iPosition;
                                    break;
                                }
                            } //Check longueur de la chaine
                        }

                        if (ret.Contains("*"))   // if focuser sends ACK command
                            bMoving = false;    // Move ended notified to client app

                        // ERROR 1 : wrong format received
                        msgLog+="PID"+PID+", ER1, retry\r\n";
                        j++;
                    }

                    if (j >= 10)
                    {
                        //msgLog+="PID"+PID+", !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  EXCEPTION - Position values wrong format. UNBALE TO UNDERSTAND HARDWARE !" + ret;
                        throw new ASCOM.InvalidValueException();
                    }

                    if (isLogEnabled)
                        File.AppendAllText(logFilePath, msgLog);
                    return iPosition;
                }
                catch (Exception ex)
                {
                    // ERROR 2 : EXCEPTION - Position value wrong format after " + j + " retry. Last message received from hardware :
                    if (isLogEnabled)
                        File.AppendAllText(logFilePath, "\r\nPID" + PID + ", ER2 " + ret + "##, P:" + ret.IndexOf("P=") + "\\r\\n" + ret.IndexOf("\n\r") + "length:" + ret.Length + "\r\n" + ex.ToString());
                    throw new ASCOM.InvalidValueException("PID"+PID+" ER2," + ret +"##, P:" + ret.IndexOf("P=") + "\\r\\n" + ret.IndexOf("\n\r")+ "length:"+ret.Length , ex);

                    //return iPosition; // retourne la dernière bonne valeur connue de position
                }
            }
        }

        
        public bool TempComp
        {
            get { return bTempComp; }
            set {
                // No Tempcomp while moving
                if (bMoving)
                    throw new ASCOM.InvalidOperationException("Focuser is Moving, please try later");

                bTempComp = value;

                if (bTempComp)
                {
                    if (isLogEnabled)
                        File.AppendAllText(logFilePath, DateTime.Now.ToLocalTime() + "\r\n---------------------------------------------------------------------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Temp comp requested \r\n");
                    string ret = CommandString("FAUTOM", true);  // command FAUTOM
                }
                else
                {
                    string ret = CommandString("FMANUA", true);  // command FMANUA
                }
            }
        }

        public bool TempCompAvailable
        {
            get { return true; }
        }

        // 08/07/2014 Nouvelle fonction plus durcie
        public double Temperature
        {
            get
            {
                int j = 0;
                string ret = "";
                int indexP = 0;
                int indexEnd = 0;
                double newTemp = 0;
                string msgLog = "";

                TID++;


                try
                {
                    // if focuser is moving, serial port is waiting for end of move ACK, send last known temp
                    //if (bMoving)
                    //    return dTemp;

                    msgLog+="\r\nTID"+ TID +"---------------------------------------------------------------------------------------\r\n";

                    while (j<=5)
                    {
                        // temperature command is FTMPRO, returns T=+/-xy.zLFCR
                        try
                        {
                            ret = CommandString("FTMPRO", true);
                        }
                        catch
                        {
                            // ERROR 3 : Exception catched Command with no answer (Temperature)
                            msgLog += "TID" + TID + "ER3, retry\r\n";
                        }
                        
                        msgLog+="TID"+ TID +" "+DateTime.Now.ToLocalTime() + " GetTemp #try" + j + " Temp=" + ret + "## T:" + ret.IndexOf("T=") + "\\r\\n" + ret.IndexOf("\n\r")+ "length:"+ret.Length+" ";

                        if (ret.IndexOf("T=") != -1 && ret.IndexOf("\n\r") != -1 && ret.IndexOf("T=") < ret.IndexOf("\n\r") && !ret.Contains("*"))
                        {
                            indexP = ret.IndexOf("T=");
                            indexEnd = ret.IndexOf("\n\r");
                            ret = ret.Substring(indexP + 2, indexEnd - indexP);
                            
                            newTemp = double.Parse(ret, CultureInfo.InvariantCulture);      // prise en compte de la décimale locale , ou .

                            msgLog+="TID"+ TID +" good format";

                            if (newTemp >= -20.0 && newTemp <= 50.0 && newTemp != -0.06)
                            {
                                dTemp = newTemp;
                                msgLog+="TID"+ TID +", good value :" + dTemp;
                                break;
                            }
                        }

                        if (ret.Contains("*"))   // if focuser sends ACK command
                            bMoving = false;    // Move ended notified to client app
                        
                        // ERROR 4 : Exception catched Command with no answer (Temperature)
                        msgLog+="TID"+ TID +", ER4, retry\r\n";
                        j++;
                    }

                    if (j >= 10)
                    {
                        //msgLog+="TID"+ TID +", CATCHED EXCEPTION - Temperature values wrong format. UNBALE TO UNDERSTAND HARDWARE !" + ret;
                        throw new ASCOM.InvalidValueException();
                    }

                    if (isLogEnabled)
                        File.AppendAllText(logFilePath, msgLog);
                    return dTemp;
                }
                catch (Exception ex)
                {
                    // ERROR 5 : CATCHED EXCEPTION - Temperature value wrong format after " + j + " retry. Last message received from hardware :
                    if (isLogEnabled)
                        File.AppendAllText(logFilePath, "\r\nTID" + TID + " ER5," + ret + "## T:" + ret.IndexOf("T=") + "\\r\\n" + ret.IndexOf("\n\r")+ "length:"+ret.Length+"\r\n" + ex.ToString());
                    //throw new ASCOM.InvalidValueException("Temperature value wrong format. Message received from hardware :" + ret, ex);

                    return dTemp; // envoie la dernière bonne valeur connue.
                }
            }
        }

        /* Tests 8/7/2014 : modification de l'algo
        public double Temperature
        {
            get{
                string ret = "";
                try
                {
                    // if focuser is moving, serial port is waiting for end of move ACK, send last known temp
                    if (bMoving)
                        return dTemp;

                    // temperature command is FTMPRO, returns T=+/-xy.zLFCR
                    ret = CommandString("FTMPRO", true);
                    
                    int indexT = ret.IndexOf("T=");
                    ret = ret.Substring(indexT + 2, ret.Length - 4);
                    dTemp = double.Parse(ret);
                    return dTemp;
                }
                catch(Exception ex)
                {
                    throw new ASCOM.InvalidValueException("Temperature value wrong format. Message received from hardware :" + ret, ex);                
                }
            }
        }*/

        #endregion
    }
}
